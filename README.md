# Thunderdome DDO Builds

DDO Builds for people on the Thunderdome server. This repository is so we can share builds easily and access the backup easily. 

Feel free to download or use any builds you see

Quick setup:
```
cd existing_build_folder
git remote add origin https://gitlab.com/jmonsky/thunderdome-ddo-builds.git
git branch -M main
git pull origin main --rebase
git add .
git commit -a
git push --set-upstream origin main
```
